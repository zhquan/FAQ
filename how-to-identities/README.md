## How to add a new organization?

An organization can be added via HatStall or SortingHat.

Go to the `Organization` page, and click the `Add` button. More details at:
https://github.com/chaoss/grimoirelab-hatstall/tree/master/docs#managing-organizations

## How to assign an e-mail domain to an organization?

The platform can map email domains to organizations. By doing that every identity with any of those mapped domains
 will be associated to the proper organization. 
 
 **Adding e-mail domains to organizations is not currently supported by Hatstall** (see
 [related Feature Request](https://github.com/chaoss/grimoirelab-hatstall/issues/68)). If you want the system to map
 the activity from an email domain to an organization please open a support ticket in GitLab and one operator will
 take care of your request.  

## How to enroll a person to an organization during a time interval?

Go to the `Profile` page of the user, click on the `Add` button. Then, you can set the start and end dates
and link the profile to an organization. More details at: https://github.com/chaoss/grimoirelab-hatstall/tree/master/docs#managing-community-profiles


![](../images/enroll-identity.png)

## How to correctly enroll a person to different organizations during consecutive time periods avoiding overlapping dates?

Dates are by default set to the beginning of the day, i.e. `00:00:00`. Thus, we should setting the periods as:
- `Organization A`: `1900-01-01` to `2019-10-01`
- `Organization B`: `2019-10-01` to `2100-01-01`

Because end date for `Organization A` will be set under the hood as `2019-10-01 00:00:00` (not inclusive) and start date
for `Organization B` will be set as `2019-10-01 00:00:00` (inclusive). 

So `2019-10-01` will be the first day
that profile is enrolled to `Organization B` and `2019-09-30` will be the last day of that profile enrolled
to `Organization A`.

It could be summarized as the following condition, where `date` is the date to check and decide the 
enrollment: `start_date <= date < end_date`.

## How to enroll a person to two organizations during the same time intervals?

Intervals should not overlap. If they do, the system won't know which organization to select for the
overlapping dates and will select one of them in a non-deterministic way. See 
[What are the implications when an identity has two enrollments with overlapping dates?](#what-are-the-implications-when-an-identity-has-two-enrollments-with-overlapping-dates) 
for more details.  

## How to un-enroll a person from an organization?

Go to the `Profile` page of the user, and click on the `un-enroll` button.

![](../images/un-enroll.png)

## What are the implications when an identity has two enrollments with overlapping dates?

When two enrollments overlap in time, the system will use one of them to update the enriched information on the
dashboard. That means that only one of them will be visible. In case you detect identities with overlapping dates edit
them so the profile is not enrolled to more than one company at the same time.

## How many types of matching do exist?

There exist 4 types of matching: `email`, `email-name`, `github`, `username`.

- `email`: two (or more identities) are matched when they share the same email address.
- `email-name`: two (or more identities) are matched when they share the same email address or their first
   and last names. See [What is the difference between matching by email and email-name?](#what-is-the-difference-between-matching-by-email-and-email-name)
   for more details on its implications.
- `github`: two (or more identities) are matched when they share the same username and both belong to a 
  `GitHub` source.
- `username`: two (or more identities) are matched when they share the same username.

## Can I apply several matching algorithms together?

Yes, you can and they will be executed sequentially. They will be applied one by one, in the same
order as they are listed.

Please fill an issue in your GitLab Support tracker to request the configuration you need.

## What is the difference between matching by email and email-name?

Let's suppose we have the following entries defined as a tuple (email, name):
```
A. Tom, ni...@acme.com
B. Jerry, jerry@acme.com
C. Jerry, ni...@acme.com
D. Tom, tom@acme.com
```

and in our identities yml file: (i) `ni...@acme.com` is the email declared for A and (ii) `jerry@acme.com` for B.

If we merge by `email`, A and C will be merged and assigned to A as follows:
```
A. Tom, ni...@acme.com
B. Jerry, jerry@acme.com
A. Jerry, ni...@acme.com
D. Tom, tom@acme.com
```

If we merge by `email-name`, A and C will be merged as shown above, and later the identities will be merged by name, thus obtaining:
```
A. Tom, ni...@acme.com
A. Jerry, jerry@acme.com <--- merged to A since the third tuple (`Jerry, ni...@acme.com`) is A
A. Jerry, ni...@acme.com
A. Tom, tom@acme.com <--- merged to A since the first tuple (`Tom, ni...@acme.com`) is A
```

Note that all identities could be assigned to B due to the following steps, after the merge by email:
```
A. Tom, ni...@acme.com
B. Jerry, jerry@acme.com
A. Jerry, ni...@acme.com
D. Tom, tom@acme.com
```

```
A. Tom, ni...@acme.com
B. Jerry, jerry@acme.com
A. Jerry, ni...@acme.com
A. Tom, tom@acme.com <-- merged by name to A based on the first tuple
```

At this point the identity A has two names (Tom and Jerry), the identity B has one name (Jerry). Thus, since both identites share the name Jerry, they can be assigned to A or B .
```
B. Tom, ni...@acme.com
B. Jerry, jerry@acme.com
B. Jerry, ni...@acme.com
B. Tom, tom@acme.com
```

## How to blacklist an email via the identities file?
In case you want to get rid of `an-annoying-email@aboring.com`, you can blacklist it via the identities file as 
show below. No matching is considered with values stored in the blacklist, that means all profiles having
the blacklisted e-mail won't be merged.

```
- blacklist:
    - an-annoying-email@aboring.com
```

Please fill an issue in your GitLab Support tracker to request the configuration you need.

## What is the organization JSON file?
The organization JSON file contains a list of organizations and their domains. It is automatically loaded
by the platform and used as starting point for affiliations.

More details on Organizations JSON file can be found on [LINK NEEDED TO SORTINGHAT DOCUMENTATION]().

Please fill an issue in your GitLab Support tracker to request the configuration you need.

## What is the identities file?
The identities file contains a list of identities with their enrollments, emails and usernames. Identities files can be 
automatically loaded into GrimoireLab and thus they will be used for matching profiles in the platform.

More details on Identities JSON file can be found on [LINK NEEDED TO SORTINGHAT DOCUMENTATION]().

Please fill an issue in your GitLab Support tracker to request the configuration you need.
