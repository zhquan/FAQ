## Contact Support

In case you find wrong data or you have any other question, please contact
us through the `support` 

You'll find a direct link named `Contact` under the `About` entry on the top menu:

![Contact Link](../images/contact.png)

Please don't select any _assignee_ of the ticket and don't use labels, we'll do it as soon as we
start processing the ticket. As soon as we start working on it you'll be notified.
