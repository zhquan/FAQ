# Updating data about contributors and companies

Examples and information needed to work with the identities file. Find [here](https://github.com/bitergia/identities) the format specification, and [here](https://gitlab.com/Bitergia/c/GrimoireLab/profiles) the own identities file for the [Grimoirelab](http://grimoirelab.github.io/) community and [dashboard](https://grimoirelab.biterg.io).

## Starting point. The identity

Consider you have the e-mail and/or username of one of your developers (or even yourself). The first step is add an entry to the `identities` file adding the name, e-mail and/or username. For example:

```yaml
- profile:
    name: Alberto Martín
  email:
    - alberto.martin@bitergia.com
  github:
    - albertinisg
```

Adding this information to the file allows [sortinghat](https://github.com/grimoirelab/sortinghat) (our identities tool) to match the entries using this name, e-mail or username in the database.

Take note that `git`, `gmane`, `jenkins`, `hyperkitty`, `mbox`, `nntp` and `pipermail` doesn't have the field username, hence the matching is done with the e-mail and/or name. For the rest of them, a section can be added in the profile, like `github` in the example above. [Here](https://github.com/grimoirelab/perceval#usage) you can find the list of supported data-sources.

## How to enroll a identity to a company

In order to add the activity of the contributor to a certain company/organization bucket, it is needed to include the name of that entity under the `enrollments` section. After adding the identity in the file like in the previous section, we can start editing the entry and add the section mentioned. The easiest scenario will be just adding the company as follows:

```yaml
- profile:
    name: Alberto Martín
  enrollments:
    - organization: Bitergia
  email:
    - alberto.martin@bitergia.com
  github:
    - albertinisg
```

And that's it! This will enroll all the activity found for that "strict" name and e-mail with the company `Bitergia`. But wait, Bitergia is a young company, and this person has been working here for few years but probably has made contributions before starting here... And those will be applied to `Bitergia`, what a mess! We need to fix that. Let's edit the file again, and select the starting date at Bitergia, as well as his previous enrollment:

```yaml
- profile:
    name: Alberto Martín
  enrollments:
    - organization: IMDEA Networks
      end: 2014-09-01
    - organization: Bitergia
      start: 2014-10-01
  email:
    - alberto.martin@bitergia.com
  github:
    - albertinisg
```

Done! Now [sortinghat](https://github.com/grimoirelab/sortinghat) will find out this, and the platform will modify the items to enroll them correctly within that timeframe. Let's go a bit further, and consider the "hipothetical scenario" that the user moves to another company. The latest edition would be:

```yaml
- profile:
    name: Alberto Martín
  enrollments:
    - organization: IMDEA Networks
      end: 2014-09-01
    - organization: Bitergia
      start: 2014-10-01
      end: 2017-11-31
    - organization: My new company
      start: 2017-12-01
  email:
    - alberto.martin@bitergia.com
  github:
    - albertinisg
```

## How to merge an identity

Ok so, after enrolling the identity correctly, we discover that the dashboard is still showing activity of the user as `unknown`. How is this possible? We've added the e-mail, name and github username, and even applied the right enrollments. What are we missing? The most common scenario is that the user has made commits with another e-mail address, like for example, his own personal account. So after we discover it, we can modify the identity again:

```yaml
- profile:
    name: Alberto Martín
  enrollments:
    - organization: IMDEA Networks
      end: 2014-09-01
    - organization: Bitergia
      start: 2014-10-01
      end: 2017-11-31
    - organization: My new company
      start: 2017-12-01
  email:
    - alberto.martin@bitergia.com
    - alberto-personal-account@example.org
  github:
    - albertinisg
```

But as we are digging with this user, we find out that we also have access to the slack and phabricator account, so we would like to have all those information unified in the same identity. The modification should be done as:

```yaml
- profile:
    name: Alberto Martín
  enrollments:
    - organization: IMDEA Networks
      end: 2014-09-01
    - organization: Bitergia
      start: 2014-10-01
      end: 2017-11-31
    - organization: My new company
      start: 2017-12-01
  email:
    - alberto.martin@bitergia.com
    - alberto-personal-account@example.org
  github:
    - albertinisg
  slack:
    - albertinisg
  phabricator:
    - albertinisg
```

## How to split an identity

Taking into account that there could be hundreds of `Alberto Martín` in the world, I've could made a mistake and the e-mail I've added doesn't really belong to that user. Don't worry! We are all humans so, in order to fix it, we are just going to split them:

```yaml
- profile:
    name: Alberto Martín
  enrollments:
    - organization: IMDEA Networks
      end: 2014-09-01
    - organization: Bitergia
      start: 2014-10-01
      end: 2017-11-31
    - organization: My new company
      start: 2017-12-01
  email:
    - alberto.martin@bitergia.com
  github:
    - albertinisg
  slack:
    - albertinisg
  phabricator:
    - albertinisg

- profile:
    name: Alberto Martín
  email:
    - alberto-personal-account@example.org
```

And that's it. Next time the file is loaded, it will break the merge done in the previous iteration and fix the identity.

## Filtering bot activity

You've probably noticed about the red tags in Git panel named `Empty Commits` and `Bots`. An explanation of what it does can be found [here](https://gitlab.com/Bitergia/c/FAQ#q-meaning-of-empty-commits-and-bots-filters), but the aim of this section is not the explanation but the configuration.

Let's say that you see activity from bots that you would like to filter out, but we need to explicitly mark it as bot in the identities file. To this aim we can do the following:

```yaml
- profile:
    name: travis-ci
    is_bot: true
  email:
    - travis@travis-ci.org
```

After loading it, the platform will find it out and set it as bot, so the filters will do the rest.

## Current Limitations


You have the following information for `Quan Zhou` in your `identities.yml` file:
```yml
- profile:
    name: Quan Zhou
  enrollments:
  - organization: Bitergia
  github:
  - quan
  email:
  - quan.zhou@bitergia.com

```

If we take that information and we add it to current people database (SortingHat), it would affiliate `github` user `quan` and `quan.zhou@bitergia.com` to `Bitergia`. Good, because that's what you want...

But, if we ask SortingHat for `Quan Zhou`, we discover that there are 3 emails, in 2 separate unique identities:
* unique identity 1687a55a368eaf8c20e4eed89378bfe40a934f9a
  * quan.zhou@bitergia.com
  * quanzhou@bitergia.com
* unique identity f6a9702eec76a6a78fa4f7248cef388ce996da0a
  * quan-personal-account@example.com

Since the `quan-personal-account@example.com` is not part of your `Quan Zhou` profile definition, it will be affiliated to `Unknown`.

Due to privacy concerns, we are not showing emails information on the dashboard. But you can build custom visualizations to check people id (author_id) and unique identities (author_uuid) in your private Kibana instance.
