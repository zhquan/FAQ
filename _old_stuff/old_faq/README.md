----

# Frequently Asked Questions

## Q. What is the format of the projects.json file?

Go to [the data sources section](../../sources) so you can read detailed information about the file used to produce yummy data with metrics.

## Q. Is this free software?

Yes. We use, write and use free/libre/open software. The product we offer is based on the [GrimoireLab](https://github.com/grimoirelab) community.

## Q. How is the Organizations pie chart shown on the Overview panel calculated?

The organizations pie used on the Overview panel is using the "git" index, which means that it is showing the affiliation information built using the data extracted from the git log + the identities database.

## Q. How do you count commits?

We count all the commits done in all the branches. What our Perceval tool get is the output of this Git command:

```
git log --raw --numstat --pretty=fuller --decorate=full --all --reverse --topo-order --parents -M -C -c --remotes=origin
```

If a commit is copied to different branches we just count it once as we count the unique hashes found in our databases.


## Q. What are the typical issues using Kibiter?

Our dashboard is based on Kibana which is a really nice application but not really friendly. These are the most common issues our users have when analyzing and interpreting data.

#### Time frame

This is the top #1. Always pay attention to the filter on top right with the selected time frame. If affects all the data shown.

#### Filters

Your panel could have some filters available. The red ones ignore data, the green ones select data and the ones discolored are ready to be used and do nothing until you enable them.

![screenshot](../images/dashboard-filters.png)

## Q. How do I know how many people left the project in Q1?

The tricky part here is .. what is your definition of "leaving the project"? We have ours but the number is not displayed in a very friendly way on the dashboard yet :(. In most of our Bitergia reports we define a contributor is inactive when she reaches 6 months of inactivity.

Let us use a metaphor here. What we need to calculate here is similar to knowing how many cans got expired from your cupboard during Q1.

![screenshot](../images/people-leaving-project.png)

Hands on, let's calculate the people who left the community during Q1 2017:
- Go to the [Git-Demographics](https://cncf.biterg.io/app/kibana#/dashboard/Git-Demographics) panel
- We'll use the chart "Last Commit Developers"
- in order to calculate Q1 2017, we need to get the people who whose last contribution to the project reached 6 months during that period of time. So, we have to count the Q1 - 6 month period, that means counting July, August and September

## Q. How can I choose/change the user:password to log in my dashboard?

You have to generate the hash of your new credentials, open a new issue in your private proyect at gitlab and tell us the hash (md5, sha1) of your selected user:password there.

To generate the hash of your credentials you can use a web service like http://www.htaccesstools.com/htpasswd-generator. You only have to write your username into the upper input box, your password into the lower one and click on the button where it is said "Create .htpasswd file". As a result, you will get a bigger text box with the hash value we need you to tell us.

## Q. Embedding a Visualization in HTML pages

If you want to share a specific visualization in a different page, please follow these steps:

- Enter on the visualization itself. You can do so in two different ways:

  - Click on `Visualize` tab in the left menu. Note that by clicking on this button you could go to the list of available visualizations or to the last one you were editing, in which case you need to click again on `Visualize` either in the breadcrumb or on the left hand side menu to go to the visualization list.
  - Click on the `Edit` button and then click on the gear icon on the top-right corner of the visualization you want to share or embed.


- Next, click on the `Share` button in the top-right corner, next to the time-picker and below the menu bar. You will see two different sections:

  - **Share saved visualization** (left): You can share or embed the most recent saved version of the selected visualization.
  ![screenshot](../images/share-visualization.png)

  - **Share Snapshot** (right): You can share a snapshot of the current view of the visualization, including filters.
  ![screenshot](../images/share-snapshot-visualization.png)


- Click on `Copy` on **Embedded iframe** section to get the iframe.
- Remove the `/edit` from the iframe URL.
- Paste it in your HTML document and it should be done!

## Q. Meaning of "Empty Commits" and "Bots" filters

**Empty commits** are commits that don't have actions over files. Examples of actions are to add (A), modify (M) or delete (D) a file. For instance, we take as example `perceval` repository:  https://github.com/grimoirelab/perceval

```
$ git log --pretty=fuller --name-status 6e7f79367244c1fa64e542b7da5a61ad1b76219c
commit 6e7f79367244c1fa64e542b7da5a61ad1b76219c
Author:     Valerio Cosentino <valcos@bitergia.com>
AuthorDate: Mon Sep 25 18:10:41 2017 +0200
Commit:     Santiago Dueñas <sduenas@bitergia.com>
CommitDate: Tue Sep 26 16:54:46 2017 +0200

    [github] Reduce requests to collect reactions on issues and comments

    This commit checks before make new requests whether there are
    reactions available.

    Backend version updated to 0.10.2.

M       perceval/backends/core/github.py
A       tests/data/github/__init__.py
R100    tests/data/github_empty_request tests/data/github/github_empty_request
R092    tests/data/github_issue_1       tests/data/github/github_issue_1
R100    tests/data/github_issue_2       tests/data/github/github_issue_2
R100    tests/data/github_issue_2_reactions     tests/data/github/github_issue_2_reactions
R100    tests/data/github_issue_comment_1_reactions     tests/data/github/github_issue_comment_1_reactions
R100    tests/data/github_issue_comment_2_reactions     tests/data/github/github_issue_comment_2_reactions
R098    tests/data/github_issue_comments_1      tests/data/github/github_issue_comments_1
R100    tests/data/github_issue_comments_2      tests/data/github/github_issue_comments_2
R098    tests/data/github_issue_expected_1      tests/data/github/github_issue_expected_1
R100    tests/data/github_issue_expected_2      tests/data/github/github_issue_expected_2
R100    tests/data/github_login tests/data/github/github_login
R100    tests/data/github_orgs  tests/data/github/github_orgs
R093    tests/data/github_request       tests/data/github/github_request
A       tests/data/github/github_request_expected
A       tests/data/github/github_request_expected_zero_reactions
R100    tests/data/github_request_from_2016_03_01       tests/data/github/github_request_from_2016_03_01
D       tests/data/github_request_expected
M       tests/test_github.py
```

This commit touches several files. It modifies `perceval/backends/core/github.py`, adds `tests/data/github/__init__.py` and deletes `tests/data/github_request_expected`, among others.

On the other hand, we have this commit:
```
$ git log --pretty=fuller --name-status 866710bb9a4d3da0c76a8d1d5f97cbe93a9fe991
commit 866710bb9a4d3da0c76a8d1d5f97cbe93a9fe991
Merge: 81f6ed4 6e7f793
Author:     Santiago Dueñas <sduenas@bitergia.com>
AuthorDate: Tue Sep 26 17:04:18 2017 +0200
Commit:     Santiago Dueñas <sduenas@bitergia.com>
CommitDate: Tue Sep 26 17:04:18 2017 +0200

    Merge branch 'github-fix-reactions' of 'https://github.com/valeriocos/perceval.git'

    Merges #163
    Closes #163

```

This is an example of empty commit (in this case is a commit that merges two branches). As you can see, it doesn't touch any file. There aren't any actions involved.

Usually merge commits are empty commits but not always. It might be conflicts between two branches and to fix these problems, the required modifications are added to the merge commit.

**bots filter** exclude commits made by bots. Each commit has a field `author_bot` that will be true if we know that user is a bot. In our identity database, we can identify bots by setting it the profile of the identity in our [identities tool](https://github.com/grimoirelab/sortinghat#basic-commands). This will modify the mentioned field, and it will filter out all those items.
