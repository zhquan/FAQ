## How to login?
The login button at bottom left corner. If you are already logged in, you will log out 
and then you will be asked to enter your credentials.

![Login Button](../images/login.png)

## How to check the data is up to date?
The `Data Status` dashboard is located on the right hand side of the top menu. There you'll find information
about when last retrieval was performed and the most recent item retrieved per each data source. If you
see something older than it should, please notify our support team via GitLab issue.

## I don't see any data in the dashboard, what do I do?
Please check that:
- The time picker located in the top left corner is properly set.
- The dashboard doesn't include any undesired filter. Filters are located on the top right corner, before 
  `Add a filter +`.
- Check the data status dashboard: [How to check the data is up to date?](#how-to-check-the-data-is-up-to-date).
 
If the checks above aren't effective, please fill an issue on your support GitLab tracker.
 
![Filters and Time Picker](../images/filters-and-time-picker.png) 

## Can't save my work, red error on top bar
Check you are correctly logged in, see [How to login?](#how-to-login).

![Can't save my work](../images/cant-save.png) 

## Can't share my work, red error on top bar
Check you are correctly logged in, see [How to login?](#how-to-login).

![Can't share my work](../images/cant-share.png) 

## How to set my own filters?
There are two main places to add filters:
- Dashboard filters. You can modify existing ones or create ne ones by clicking on `Add a filter +`.
- Time picker allows you to set a time frame for visualizing data. Most dashboards are based on item
  creation date. That means they will be count/drawn at the time they were created.
  
![Filters and Time Picker](../images/filters-and-time-picker.png)  

Nevertheless, almost any widget will allow you to click on slices, names (or small magnifying glass icons that 
will appear when hovering or clicking on names), or drag and drop across chart x-axis. All these actions
will add the corresponding filters on top or will modify the time picker accordingly.

![Actionable widgets](../images/actionable-widgets.png) 

## What is an index pattern?
An index pattern shows the different attributes contained in an index or a set of them. It allows to apply
some formatting on top of Elasticsearch indexes or aliases (date formatting, string truncation, formatting
strings as links, etc.). Data types are automatically inferred from Elasticsearch index mappings. It also
allows to group several physical indexes. 

Everything in Kibana is built on top of Index Patterns, they are the views Kibana provides over the actual
Elasticsearch indexes.

More details can be found on [Kibana official documentation](https://www.elastic.co/guide/en/kibana/6.1/index-patterns.html).

## What is an index?
An index is a collection of JSON documents.

## Where can we find a description of the different indexes?
[GrimoireLab-ELK](https://github.com/chaoss/grimoirelab-elk) is the tool used for creating enriched indexes.
Thus, it is the place for looking documentation related to indexes. In this particular case, 
[schema directory](https://github.com/chaoss/grimoirelab-elk/tree/master/schema) contains a set of CSV
files (one per index) with a list of fields and their description.

If you find any missing piece of information or something misleading, please 
[fill an issue in GrimoireLab-ELK project](https://github.com/chaoss/grimoirelab-elk/issues) requesting
the change or update.


## How to inspect an index from Kibiter?
An index can be queried via the `Discover` in Kibiter. To access the `Discover`, click on the `compass` icon at the top
left corner, just below the owl.

## How to query Elasticsearch from Kibiter?
Operations on the Elasticsearch indexes can be achieved via the `Dev tools`. To access the `Dev tools`, click on the `wrench` icon
on the left, just above the gear.

## How to share a dashboard?
- [Login to Kibiter](#how-to-login)
- [Set a time interval and filters](#how-to-set-my-own-filters) (optionally)
- Click on the `Share` tab on top right corner
- A menu with two main sections will drop down. 
  - On the left you can share the dashboard as it is saved.
  - On the right you can share a snapshot of the dashboard, that is, the dashboard as it is displayed 
   (same filters, same time period, same layout). This link will work regardless the saved version of the
   dashboard. This is due to the fact that Kibana stores everything in the URL payload, so by sharing
   a URL you are sharing dashboard's current configuration and layout too.
- For screenshots you can also shorten the link. In the section `Link`, click on `Short URL`
- Click on `Copy` link just above the link you want to share.
- Share the link!

![Share Dashboard](../images/share-dashboard.png) 


## How to import/export dashboards?
Import and export operations can be achieved via [Kidash](https://github.com/chaoss/grimoirelab-kidash).

- Export
    
    The dashboard with ID `Git` located at `http://localhost:9200` will be saved to `./git.json`. If the flag
    `--split-index-patterns` is set, the index pattern will be exported in the same folder where the `git.json` is
    saved, otherwise it will be embedded in `git.json`.
    ```
    kidash -g -e <Elasticsearch-url> --dashboard <dashboard-id>* --export <local-file-path>
    ex.: kidash -g -e http://localhost:9200 --dashboard Git --export ./git.json --split-index-patterns
    ```
  
- Import

    The dashboard included in `./git.json` will be uploaded to `http://localhost:9200`. The same command
    can be used to import an index pattern.
    ```
    kidash -g -e <Elasticsearch-url> --import <local-file-path>
    ex.: kidash -g -e http://localhost:9200 --import ./git.json
    ```

## How to properly save a new visualization or dashboard?
Standard dashboards provided by Bitergia offers a lot of information, however you may wish to create your
own visualizations or/and dashboards or edit some of the existing ones. This is perfectly fine, you just
need to take care of saving it properly to avoid Bitergia to overwrite your work. This basically could
happen if you store your work overwriting some standard dashboard or visualization.

The steps you should follow to make sure your work is safely saved are:
- Click on `Save` 

![Save](../images/save.png) 
- Make sure `Save as new dashboard` or `Save as new visualization` checkbox is marked.

![Save Dashboard](../images/save-dashboard.png) 

![Save Visualization](../images/save-visualization.png) 

- Give your dashboard or visualization a new name that allows you to find it later. For Dashboards, Bitergia
ones always contain the word `Bitergia` within the description text.



## How to edit a visualization?
- [Login to Kibiter](#how-to-login).
- Click on the `Edit` tab on top right corner (gears should appear on the top left corner of each visualization).
- Click on the `gear` icon of the target visualization.
- Click on `Edit visualization`.
- Follow [How to properly save a new visualization or dashboard?](#how-to-properly-save-a-new-visualization-or-dashboard)
instructions if you are editing an existing dashboard/visualization not originally created by you. If you are
modifying your own object, just overwrite it by clicking on the button `Save` on the top right corner.

## How to edit the name of a visualization?
- [Login to Kibiter](#how-to-login).
- Click on the `Edit` tab on top right corner (gears should appear on the top left corner of each visualization).
- Click on the `gear` icon of the target visualization.
- Click on `Customize panel` and change the title.
- Once done,  follow [How to properly save a new visualization or dashboard?](#how-to-properly-save-a-new-visualization-or-dashboard)
instructions if you are editing an existing dashboard/visualization not originally created by you. If you are
modifying your own object, just overwrite it by clicking on the button `Save` on the top right corner.

## How to create a visualization?
- [Login to Kibiter](#how-to-login).
- Click on the `bar chart` icon at the top left corner, just below the `compass` icon. If a visualization
shows up, click again on the same icon until you get to the following screen:

![New Visualization](../images/new-visualization.png) 

- Click on the `plus` button just about the table listing the current visualizations.

![New Visualization Plus Icon](../images/new-visualization-plus.png) 

- Select a visualization type.
- Select an index pattern to create your visualization on top of it.
- After creating your visualization, click on the button `Save` on the top right corner.

## How to edit a dashboard?
- [Login to Kibiter](#how-to-login).
- Go to the target dashboard.
- Click on the `Edit` tab on top right corner.
- Click on the `Add` tab on top right corner.
- Create a new visualization or load an existing one.
- Once done, the visualization will be visible on the dashboard.
- Move/resize the visualizations in the dashboard, set filters and time picker (optionally).
- Once done,  follow [How to properly save a new visualization or dashboard?](#how-to-properly-save-a-new-visualization-or-dashboard)
instructions if you are editing an existing dashboard/visualization not originally created by you. If you are
modifying your own object, just overwrite it by clicking on the button `Save` on the top right corner.

## How to create a dashboard?
- [Login to Kibiter](#how-to-login).
- Click on the third icon at the top left corner, just below the `compass` and `bar chart` icons.

![New Dashboard](../images/new-dashboard.png) 

- Click on the `plus` button just about the table listing the current dashboards.
- Add visualizations by clicking on the `Add` tab on top right corner.
- Once done, click on the button `Save` on the top right corner.

## How to change the top menu?
By default Bitergia provides a top menu configured for accessing all dashboards available in your
standard or customized installation of the platform. The usual menu looks like the following:

![Top Menu](../images/top-menu.png)

Nevertheless, you don't need to stick to this menu. Although there currently isn't any way to modify
the menu from the dashboard itself, you can ask us to modify the menu. Just open an issue on your
GitLab support tracker and let us know the menu contents you would like to have. Note that any dashboard
can be linked from the menus, so you can include your own customized dashboards and not only Bitergia
standard ones.

## Author_org_name and author_org_name, what's the difference?
The attribute `Author_org_name` stores the same info of the attribute `author_org_name`. The latter are used in the current visualizations and are the ones that should be used to build new visualizations.