## Setup your Gitlab notification preferences

In order to setup your Gitlab notifications go to URL of the project you are interested in following (E.g. https://gitlab.com/Bitergia/c/GrimoireLab) and click on the bell icon. If you need more help about the notification options see the [following link]( https://gitlab.com/help/workflow/notifications.md#group-settings)

In case you are only interested in a specific repository under your project, you can also do this at repository level. (E.g. people only interested in tracking the support issues)
