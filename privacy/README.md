## Privacy

### Access to the dashboard

There are two main roles, the user with permission to see the dashboard and
the user with permission to edit it and accessing the indexes.

In case you want to change the visibility of the dashboard (or the indexes), contact the support team via ticket and let them know. See [how to contact support].


### Define who can access the data at Gitlab

Depending on your scope we recommend you to choose between three different configurations.

* _private_: everything private, only people with access to the repos will have access to the information. No tickets can be opened by external contributors.
* _normal_: the `profiles` repository remains private so in case you submit information about contributors it won't be visible. The rest of the trackers would be public so your community members will be able to provide feedback via issues, creating pull requests to add more organization-domain mappings to the dashboard or adding new repositories
* _public_: all information is publicly available. This is the ideal scenario to have more contributions from your community members. Same as 'normal' but community members will be also able to contribute to the profiles information, so
they will be able to improve/fix information about themselves or their team (everything via Pull Requets by default)

If you are community manager of a libre/open source community we strongly recommend **normal** or **public**

[how to contact support]: gitlab_contact_support/README.md

