
## Bitergia Analytics FAQ

1. General
    1. [How to contact support]
    1. [How to create a Merge Request]
    1. [Set up your Privacy settings]
    1. [Set up your Notification preferences]
    1. [Structure of the Gitlab repos]
1. Identities
    1. [How to add a new organization?]
    1. [How to assign an e-mail domain to an organization?]
    1. [How to enroll a person to an organization during a time interval?]
    1. [How to un-enroll a person from an organization?]
    1. [How to correctly enroll a person to different organizations during consecutive time periods avoiding overlapping dates?]
    1. [How to enroll a person to two organizations during the same time intervals?]
    1. [What are the implications when an identity has two enrollments with overlapping dates?]
    1. [How many types of matching do exist?]
    1. [Can I apply more matching algorithms?]
    1. [What is the difference between matching by email and email-name?]
    1. [How to blacklist an email via the identities file?]
    1. [What is the organization JSON file?]
    1. [What is the identities file?]
1. Data sources management
    1. [What is the projects.json?]
    1. [What is the setup.cfg?]
    1. [How do the setup.cfg and projects.json relate each other?]
    1. [How to add a new repository?]
    1. [How to enrich only a part of the raw data?]
    1. [How to keep a repository in the projects.json when it doesn't exist anymore in upstream?]
    1. [How to add labels to repositories?]
1. Dashboards
    1. [How to login?]
    1. [How to check the data is up to date?]
    1. [I don't see any data in the dashboard, what do I do?]
    1. [Can't save my work, red error on top bar]
    1. [Can't share my work, red error on top bar]
    1. [How to set my own filters?]
    1. [What is an index pattern?]
    1. [What is an index?]
    1. [Where can we find a description of the different indexes?]
    1. [How to inspect an index from Kibiter?]
    1. [How to query elasticsearch from Kibiter?]
    1. [How to share a dashboard?]
    1. [How to import/export dashboards?]
    1. [How to properly save a new visualization or dashboard?]
    1. [How to edit a visualization?]
    1. [How to edit the name of a visualization?]
    1. [How to create a visualization?]
    1. [How to edit a dashboard?]
    1. [How to create a dashboard?]
    1. [How to change the top menu?]
    1. [Author_org_name and author_org_name, what's the difference?]

[How to contact support]:gitlab_contact_support/README.md
[How to create a Merge Request]:how-to-mr/README.md
[Set up your Privacy settings]:privacy/README.md
[Set up your Notification preferences]:gitlab_notification/README.md
[Structure of the Gitlab repos]:gitlab_repos/README.md
    
[How to add a new organization?]:how-to-identities/README.md#how-to-add-a-new-organization
[How to assign an e-mail domain to an organization?]:how-to-identities/README.md#how-to-assign-an-e-mail-domain-to-an-organization
[How to enroll a person to an organization during a time interval?]:how-to-identities/README.md#how-to-enroll-a-person-to-an-organization-during-a-time-interval    
[How to un-enroll a person from an organization?]:how-to-identities/README.md#how-to-un-enroll-a-person-from-an-organization
[How to correctly enroll a person to different organizations during consecutive time periods avoiding overlapping dates?]:how-to-identities/README.md#how-to-correctly-enroll-a-person-to-different-organizations-during-consecutive-time-periods-avoiding-overlapping-dates
[How to enroll a person to two organizations during the same time intervals?]:how-to-identities/README.md#how-to-enroll-a-person-to-two-organizations-during-the-same-time-intervals
[What are the implications when an identity has two enrollments with overlapping dates?]:how-to-identities/README.md#what-are-the-implications-when-an-identity-has-two-enrollments-with-overlapping-dates
[How many types of matching do exist?]:how-to-identities/README.md#how-many-types-of-matching-do-exist
[Can I apply more matching algorithms?]:how-to-identities/README.md#can-i-apply-more-matching-algorithms
[What is the difference between matching by email and email-name?]:how-to-identities/README.md#what-is-the-difference-between-matching-by-email-and-email-name
[How to blacklist an email via the identities file?]:how-to-identities/README.md#how-to-blacklist-an-email-via-the-identities-file
[What is the organization JSON file?]:how-to-identities/README.md#what-is-the-organization-json-file
[What is the identities file?]:how-to-identities/README.md#what-is-the-identities-file

[What is the projects.json?]:how-to-sources/README.md#what-is-the-projectsjson
[What is the setup.cfg?]:how-to-sources/README.md#what-is-the-setupcfg
[How do the setup.cfg and projects.json relate each other?]:how-to-sources/README.md#how-do-the-setupcfg-and-projectsjson-relate-each-other
[How to add a new repository?]:how-to-sources/README.md#how-to-add-a-new-repository
[How to enrich only a part of the raw data?]:how-to-sources/README.md#how-to-enrich-only-a-part-of-the-raw-data
[How to keep a repository in the projects.json when it doesn't exist anymore in upstream?]:how-to-sources/README.md#how-to-keep-a-repository-in-the-projectsjson-when-it-doesnt-exist-anymore-in-upstream
[How to add labels to repositories?]:how-to-sources/README.md#how-to-add-labels-to-repositories

[How to login?]:how-to-dashboard/README.md#how-to-login
[How to check the data is up to date?]:how-to-dashboard/README.md#how-to-check-the-data-is-up-to-date
[I don't see any data in the dashboard, what do I do?]:how-to-dashboard/README.md#i-dont-see-any-data-in-the-dashboard-what-do-i-do
[Can't save my work, red error on top bar]:how-to-dashboard/README.md#cant-save-my-work-red-error-on-top-bar
[Can't share my work, red error on top bar]:how-to-dashboard/README.md#cant-share-my-work-red-error-on-top-bar
[How to set my own filters?]:how-to-dashboard/README.md#how-to-set-my-own-filters
[What is an index pattern?]:how-to-dashboard/README.md#what-is-an-index-pattern
[What is an index?]:how-to-dashboard/README.md#what-is-an-index
[Where can we find a description of the different indexes?]:how-to-dashboard/README.md#where-can-we-find-a-description-of-the-different-indexes
[How to inspect an index from Kibiter?]:how-to-dashboard/README.md#how-to-inspect-an-index-from-kibiter
[How to query elasticsearch from Kibiter?]:how-to-dashboard/README.md#how-to-query-elasticsearch-from-kibiter
[How to share a dashboard?]:how-to-dashboard/README.md#how-to-share-a-dashboard
[How to import/export dashboards?]:how-to-dashboard/README.md#how-to-importexport-dashboards
[How to properly save a new visualization or dashboard?]:how-to-dashboard/README.md#how-to-properly-save-a-new-visualization-or-dashboard
[How to edit a visualization?]:how-to-dashboard/README.md#how-to-edit-a-visualization
[How to edit the name of a visualization?]:how-to-dashboard/README.md#how-to-edit-the-name-of-a-visualization
[How to create a visualization?]:how-to-dashboard/README.md#how-to-create-a-visualization
[How to edit a dashboard?]:how-to-dashboard/README.md#how-to-edit-a-dashboard
[How to create a dashboard?]:how-to-dashboard/README.md#how-to-create-a-dashboard
[How to change the top menu?]:how-to-dashboard/README.md#how-to-change-the-top-menu
[Author_org_name and author_org_name, what's the difference?]:how-to-dashboard/README.md#author_org_name-and-author_org_name-whats-the-difference






